const express = require('express');
const fs = require('fs');
const path = require('path');
const morgan = require('morgan');
const cors = require('cors');
const filesRouter = require('./filesRouter');

const app = express();

app.use(cors());
app.use(express.json());
app.use(morgan('tiny'));

fs.stat(path.join(__dirname, './files'), e => {
	if (e) {
		fs.mkdir('files', () => {});
	}
});

app.use('/api', filesRouter);

app.listen(8080, () => {
	console.log('Server started', new Date());
});
