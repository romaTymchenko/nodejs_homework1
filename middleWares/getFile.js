const fs = require('fs');
const path = require('path');

module.exports.getFile = (req, res) => {
	const { filename } = req.params;
	if (fs.existsSync('files/' + filename)) {
		console.log('GO', 'files/' + filename);
		fs.readFile(path.join(__dirname, '../files', filename), (err, data) => {
			if (err) {
				return res.status(500).json({ message: 'Server error' });
			}
			fs.stat('files/' + filename, (error, stats) => {
				if (error) return res.status(500).json({ message: 'Server error' });
				return res.status(200).json({
					message: 'Success',
					filename: filename,
					content: Buffer.from(data).toString(),
					extension: path.extname(filename).slice(1),
					uploadedDate: stats.mtime,
				});
			});
		});
	} else {
		return res.status(400).json({ message: `No file with '${filename}' filename found` });
	}
};
