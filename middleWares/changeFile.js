const fs = require('fs');
const path = require('path');

module.exports.changeFile = (req, res) => {
	const oldFileName = req.params.filename;
	const { filename, content } = req.body;

	fs.readdir(path.join(__dirname, '../files'), (err, files, next) => {
		if (err) {
			return res.status(500).json({ message: 'Server error' });
		}

		if (files.indexOf(oldFileName) === -1) return res.status(400).json({ message: 'File does not exist' });

		if (content) {
			fs.writeFile(path.join(__dirname, '../files', oldFileName), content, err => {
				if (err) return res.status(500).json({ message: 'Server error' });
			});
		}

		if (filename && filename !== oldFileName) {
			const extName = path.extname(filename);
			if (extName === '.js' || extName === '.xml' || extName === '.yaml' || extName === '.txt' || extName === '.log' || extName === '.json') {
				fs.rename(path.join(__dirname, '../files', oldFileName), path.join(__dirname, '../files', filename), err => {
					if (err) return res.status(500).json({ message: 'Server error' });
				});
			} else {
				return res.status(400).json({ message: 'File has not supported extension' });
			}
		}
		return res.status(200).json({ message: 'File has been changed' });
	});
};
