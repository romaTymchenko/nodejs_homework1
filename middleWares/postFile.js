const fs = require('fs');
const path = require('path');

module.exports.postFile = (req, res) => {
	const { filename, content } = req.body;

	if (!filename) {
		return res.status(400).json({ message: "Please make file with 'filename'" });
	}
	if (!content) {
		return res.status(400).json({ message: "Please specify 'content' parameter" });
	}

	fs.readdir(path.join(__dirname, '../files'), (err, files) => {
		if (err) {
			return res.status(500).json({ message: 'Server error' });
		}
		const newFile = files.find(item => item == filename);
		const extName = path.extname(filename);
		if (newFile === filename) return res.status(400).json({ message: 'File is already exist' });
		if (extName === '.js' || extName === '.xml' || extName === '.yaml' || extName === '.txt' || extName === '.log' || extName === '.json') {
			fs.writeFile(path.join(__dirname, '../files', filename), content, err => {
				if (err) {
					return res.status(500).json({ message: 'Server error' });
				}
				return res.status(200).json({ message: 'File created successfully' });
			});
		} else {
			return res.status(400).json({ message: 'File has not supported extension' });
		}
	});
};
