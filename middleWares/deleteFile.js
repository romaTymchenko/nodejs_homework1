const fs = require('fs');
const path = require('path');

module.exports.deleteFile = (req, res) => {
	const { filename } = req.params;
	fs.unlink('files/' + filename, err => {
		if (err && err.code == 'ENOENT') {
			return res.status(400).json({ message: 'File does not exist' });
		} else if (err) {
			return res.status(500).json({ message: 'Server error' });
		} else {
			return res.status(200).json({ message: 'File has been deleted' });
		}
	});
};
