const fs = require('fs');
const path = require('path');

module.exports.getALlFiles = (req, res) => {
	fs.readdir(path.join(__dirname, '../files'), (err, files) => {
		if (err) {
			return res.status(500).json({ message: 'Server error' });
		}
		if (files.length === 0) {
			console.log('Folder is empty');
			return res.status(400).json({ message: 'Client error' });
		}
		return res.status(200).json({ message: 'Success', files });
	});
};
