const express = require('express');

const router = express.Router();

const { getALlFiles } = require('./middleWares/getAllFiles');
const { getFile } = require('./middleWares/getFile');
const { postFile } = require('./middleWares/postFile');
const { changeFile } = require('./middleWares/changeFile');
const { deleteFile } = require('./middleWares/deleteFile');

router.get('/files', getALlFiles);
router.get('/files/:filename', getFile);
router.post('/files', postFile);
router.put('/files/:filename', changeFile);
router.delete('/files/:filename', deleteFile);

module.exports = router;
